<?php

/**  
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; under version 2
 * of the License (non-upgradable).
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 * Copyright (c) 2022 (original work) Open Assessment Technologies SA;
 *               
 * 
 */

namespace oat\userInfo\controller;


use core_kernel_classes_Property;
use tao_models_classes_UserService;
use oat\generis\model\user\UserRdf;
use oat\oatbox\log\LoggerAwareTrait;
use oat\generis\model\OntologyAwareTrait;
use oat\oatbox\event\EventManagerAwareTrait;
use oat\tao\helpers\UserHelper;

/**
 * Sample controller
 *
 * @author Open Assessment Technologies SA
 * @package userInfo
 * @license GPL-2.0
 *
 */
class UserInfo extends \tao_actions_CommonModule
{
    use EventManagerAwareTrait;
    use OntologyAwareTrait;
    use LoggerAwareTrait;

    /**
     * initialize the services
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * A possible entry point to tao
     */
    public function index()
    {
    }

    public function getUserInfo()
    {
        $uri = '';
        if ($this->hasRequestParameter('uri')) {
            $uri = $this->getRequestParameter('uri');

            $user = UserHelper::getUser($uri);

            $UserData = $this->getUserDataArray($uri, $user);

            $this->setData('UserData', $UserData);
            $this->setView('UserInfo/Users-details.tpl');
        } else {
            echo ucfirst($uri) . ', may the force be with you';
        }
    }

    public function getAllUsersNameJSON()
    {

        $ServiceLocator = $this->getServiceLocator();
        $userService = $ServiceLocator->get(tao_models_classes_UserService::class);
        $users = $userService->getAllUsers();
        $data = array(
            'data'  => __("Users By login name"),
            'attributes' => array(
                'id' => 1,
                'class' => 'node-class'
            ),
            'children' => array()
        );

        foreach ($users as $user) {
            $uri = $user->getUri();
            $UserData = $this->getUserDataArray($uri, $user);
            $UserFullName = $UserData['login'];

            $data['children'][] =  array(
                'data'  => ucfirst($UserFullName),
                'attributes' => array(
                    'id' =>  $uri,
                    'class' => 'node-instance'
                )
            );
        }

        echo json_encode($data);
    }

    private function getUserDataArray($uri, $user)
    {
        $rolesProperty = new core_kernel_classes_Property(UserRdf::PROPERTY_ROLES);

        $UIuserLang = null;
        $DefuserLang = null;

        $UIuserLang = new core_kernel_classes_Property(reset($user->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_UILG))));
        $UIuserLang = (string) $this->getResource($UIuserLang)->getLabel();

        if (!empty($user->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_DEFLG)))) {
            $DefuserLang = new core_kernel_classes_Property(reset($user->getPropertyValues(new core_kernel_classes_Property(PROPERTY_USER_DEFLG))));
            $DefuserLang = (string)  $this->getResource($DefuserLang)->getLabel();
        }


        $roles = $user->getPropertyValues($rolesProperty);
        $RolesLabel = [];

        foreach ($roles as $RoleURI) {
            $RolesLabel[] = $this->getResource($RoleURI)->getLabel();
        }

        $UserResource = UserHelper::getUser($user);

        $UserDataArray = [
            'id' => $uri,
            'login' => UserHelper::getUserLabel($UserResource),
            'firstname' => UserHelper::getUserFirstName($UserResource),
            'lastname' => UserHelper::getUserLastName($UserResource),
            'email' => UserHelper::getUserMail($UserResource),
            'ui_lang' => empty($UIuserLang) ? null : $UIuserLang,
            'default_lang' => empty($DefuserLang) ? null : $DefuserLang,
            'roles' => $RolesLabel,
        ];

        return $UserDataArray;
    }
}
