# Tao User Lister and detail viewer [Sayegh1944] `v0.2`

TAO extenision that list user and view thier details.


## Usage

`Under Construction`

1. Download `taoDevTools` extension using composer `composer require oat-sa/extension-tao-devtools:3.4.0` then enable extenssision.
2. Edit the other `composer.json` at the root level of your TAO installation. There are 2 parts you will need to add:
At the top level, a new repository definition:

```
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/sayegh1944/userinfo.git"
        }
    ],
```
And within the existing require block:

```
    "require" : {
        // long list of other repos,
        "sayegh1944/userinfo" : "dev-master"
    }

```
3. You are now finally ready to run `composer update` which should find your package on Github and pull it in.


4. Go to Extenision manager and install **userInfo**  extenision.
   

### TO-DO List
- [x] Refactor code
- [X] Refactor the GIT repository
- [X] Write Documentation
