<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User List</title>
    <style type="text/css">
        .form-style-1 {
            margin: 10px auto;
            padding: 20px 12px 10px 20px;
            font: 13px "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

        .form-style-1 li {
            padding: 0;
            display: block;
            list-style: none;
            margin: 10px 0 0 0;
        }

        .form-style-1 label {
            margin: 0 0 3px 0;
            padding: 0px;
            display: block;
            font-weight: bold;
        }

        .form-style-1 input[type=text],
        textarea {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            border: 1px solid #BEBEBE;
            padding: 7px;
            margin: 0px;
            outline: none;
        }


        .form-style-1 .field-divided {
            width: 49%;
        }

        .form-style-1 .field-long {
            width: 100%;
        }

        .form-style-1 .field-textarea {
            height: 100px;
        }
    </style>
</head>

<body>
    <form style="width: 100%; max-width:500px;">
        <h1>User Details</h1>

        <ul class="form-style-1">
            <li>
                <label>Resource Identifier </label>
                <input type="text" class="field-long" disabled value="<?= $UserData['id'] ?>" />
            </li>
            <li>
                <label>Login Name </label>
                <input type="text" class="field-long" disabled value="<?= $UserData['login'] ?>" />
            </li>
            <li>
                <label>Last Name </label>
                <input type="text" class="field-long" disabled />
            </li>
            <li>
                <label>Email</label>
                <input type="text" class="field-long" disabled value="<?= $UserData['email'] ?>" />
            </li>
            <li>
                <label>UI Interface Language</label>
                <input type="text" class="field-long" disabled value="<?= $UserData['ui_lang'] ?>" />
            </li>
            <li>
                <label>Default Interface Language</label>
                <input type="text" class="field-long" disabled value="<?= $UserData['default_lang'] ?>" />
            </li>
            <li>
                <label>Roles</label>
                <textarea class="field-long field-textarea" disabled><?php foreach ($UserData['roles'] as $key => $value) {
                                                                                                    echo $value . "\n";
                                                                                                } ?></textarea>
            </li>


        </ul>
    </form>
    </div>


</body>

</html>